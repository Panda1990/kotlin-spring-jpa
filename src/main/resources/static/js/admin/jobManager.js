var JobManager = {
		jobTable: {},
		baseURL: {},
		selectedJob: {},
		init: function (tableSelector) {
			var getUrl = window.location;
			var baseUrl = getUrl .protocol + "//" + getUrl.host + "/";
			JobManager.baseURL = baseUrl;
			$("#add-new-job").click (function (e){
				e.preventDefault();
				var button = this;
				button.busy = true;
				JobManager.saveNewJob();
			});
			$("#cancelModalButton").click (function (e){
				e.preventDefault();
				var button = this;
				button.busy = true;
				document.getElementById("deleteModal").style.display = "none";
			});
			$("#cancelEditModalButton").click (function (e){
				e.preventDefault();
				var button = this;
				button.busy = true;
				document.getElementById("editModal").style.display = "none";
			});
			JobManager.initjobTable (tableSelector);
		},
		deleteJob: function (job) {
			console.log("Waiting...");
			return jQuery.ajax({
				type: "DELETE",
				url: JobManager.baseURL + "rest/job",
				data: {
					id: job.id
				},
				dataType: "json",
				ContentType: "application/json",
				success: function (data, textStatus, jQxhr) {
				},
				error: function (jqXhr, textStatus, errorThrown) {
				}
			})
				.always(function (jqXhr, textStatus, errorThrown) {
					JobManager.jobTable.ajax.reload(null, false);
				});
		},
		saveNewJob: function () {
			console.log("Waiting...");
			var today = new Date();
			var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
			if (document.getElementById('startDate').value != ""){
				date = document.getElementById('startDate').value;
			}
			return jQuery.ajax({
				type: "POST",
				url: JobManager.baseURL + "rest/job",
				data: {
					customerID: document.getElementById('customer').value,
					productID: document.getElementById('product').value,
					price: document.getElementById('price').value,
					startDate: date,
					pickUpDate: document.getElementById('pickUpDate').value,
					description: document.getElementById('description').value
				},
				dataType: "json",
				ContentType: "application/json",
				success: function (data, textStatus, jQxhr) {
					JobManager.jobTable.ajax.reload(null, false);
				},
				error: function (jqXhr, textStatus, errorThrown) {
					console.log ("error");
				}
			})
				.always(function (jqXhr, textStatus, errorThrown) {
				});
		},
		editJob: function (id) {
			console.log("Waiting...");
			var today = new Date();
			var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
			if (document.getElementById('editStartDate').value != ""){
				date = document.getElementById('editStartDate').value;
			}
			return jQuery.ajax({
				type: "PUT",
				url: JobManager.baseURL + "rest/job",
				data: {
					id: id,
					customerID: document.getElementById('editCustomer').value,
					productID: document.getElementById('editProduct').value,
					price: document.getElementById('editPrice').value,
					startDate: date,
					pickUpDate: document.getElementById('editPickUpDate').value,
					description: document.getElementById('editDescription').value
				},
				dataType: "json",
				ContentType: "application/json",
				success: function (data, textStatus, jQxhr) {
					JobManager.jobTable.ajax.reload(null, false);
				},
				error: function (jqXhr, textStatus, errorThrown) {
					console.log ("error");
				}
			})
				.always(function (jqXhr, textStatus, errorThrown) {
				});
		},
		initjobTable (tableSelector){
			JobManager.jobTable=$(tableSelector).DataTable({
					"processing": true,
			        "serverSide": true,
			        "ajax": {
			            "url": JobManager.baseURL+"datatable/jobs",
			            "type": "GET",
			            "dataSrc": function ( json ) {
							jQuery.ajaxSettings.traditional = true;
							return json.data;
						},
						"data": function ( d ) {
							jQuery.ajaxSettings.traditional = false;
						}
			        },
		        	columns: [
		        		{ "data": "id" },
		        		{ "data": "customer" , "className": 'centered-column'},
				        { "data": "product" , "className": 'centered-column'},
				        { "data": "description" , "className": 'centered-column'},
				        { "data": "startJobDate" , "className": 'centered-column'},
		        		{ "data": "pickUpDate" },
		        		{ "data": "completing", "className": 'centered-column'},
		        		{ "data": "price", "className": 'centered-column'},
				        { "data": null, "dafaultContent" : false , "className": 'centered-column'}
				     ],
					columnDefs: [
						{
							render: function (data, type, row){
								return data.firstName + " " + data.lastName;
							},
							targets: [ 1 ]
						},
						{
							render: function (data, type, row){
								return data.name;
							},
							targets: [ 2 ]
						},
						{
							render: function (data, type, row){
								if (data == null){
									return '<span class="aui-lozenge aui-lozenge-current">NOT AVAILABLE</span>';
								}
								var date = new Date(data);
								return date;
							},
							targets: [ 4, 5 ]
						},
						{
							render: function (data, type, row){
								return data + " €"
							},
							targets: [ 7 ]
						},
						{
							render: function ( data, type, row ) {
								return '<button class="titled delete-job-button" type="button" id="deleteJob" title="Delete Job"><span class="fa fa-trash" aria-hidden="true"></span></button>';
							},
							targets: 8
						},
						{
							render: function ( data, type, row ) {
								return '<button class="titled edit-job-button" type="button" id="editJob" title="Edit Job"><span class="fa fa-pencil" aria-hidden="true"></span></button>';
							},
							targets: 9
						},
						{ visible: false,  targets: [0] },
						{ orderable: false,  targets: [ 8, 9] }
			        ],
			        order: [[ 0, 'asc' ]]
		    	});
			$(tableSelector + " tbody").on ('click', '#deleteJob', function() {
		    	var tableRow = JobManager.jobTable.row($(this).parents('tr') );
		   		var job = tableRow.data();
		   		JobManager.selectedJob = job;
		   		JobManager.showDialog (job);
				});
			$(tableSelector + " tbody").on ('click', '#editJob', function() {
		    	var tableRow = JobManager.jobTable.row($(this).parents('tr') );
		   		var job = tableRow.data();
		   		JobManager.selectedJob = job;
		   		JobManager.showEditDialog (job);
				});
			},
			showDialog : function (job) {
				var modal = document.getElementById("deleteModal");
				modal.style.display = "block";
				$("#deleteModalButton").click (function (e){
					e.preventDefault();
					var button = this;
					button.busy = true;
					JobManager.deleteJob (job);
					modal.style.display = "none";
				});
			},
				showEditDialog : function (job){
					var editModal = document.getElementById("editModal");
					
					$("#editCustomer").val(job.customer.id);
					$("#editProduct").val(job.product.id);
					$("#editPrice").val(job.price);
					$("#editPickUpDate").val(job.pickUpDate);
					$("#editStartDate").val(job.startDate);
					$("#editDescription").val(job.description);
					
					editModal.style.display = "block";
					$("#edit-job").click (function (e){
						e.preventDefault();
						var button = this;
						button.busy = true;
						JobManager.editJob (job.id);
						document.getElementById("editModal").style.display = "none";
					});
				}
}