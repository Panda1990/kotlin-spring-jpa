var NavManager = {
	baseURL : {},
	init : function() {
		const getUrl = window.location;
		const baseUrl = getUrl.protocol + "//" + getUrl.host + "/";
		console.log (getUrl);
		const urlIdentifier = getUrl.pathname.split('/')[1];

		if (urlIdentifier == "jobs"){
			$("#jobLink").addClass("active");
		} else if (urlIdentifier == "products"){
			$("#productLink").addClass("active");
		} else if (urlIdentifier == "customers") {
			$("#customerLink").addClass("active");
		} else if (urlIdentifier == "commerce") {
			$("#commerceLink").addClass("active");
		} else {
			$("#homeLink").addClass("active");
		}
		NavManager.baseURL = baseUrl;
		$("#jobLink").click(function() {
			window.location = NavManager.baseURL + "jobs";
		});
		$("#productLink").click(function() {
			window.location = NavManager.baseURL + "products";
		});
		$("#customerLink").click(function() {
			window.location = NavManager.baseURL + "customers";
		});
		$("#homeLink").click(function() {
			window.location = NavManager.baseURL;
		});
		$("#commerceLink").click(function() {
			window.location = NavManager.baseURL + "commerce";
		});
	}
}