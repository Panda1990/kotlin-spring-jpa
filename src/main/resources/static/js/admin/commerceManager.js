var CommerceManager = {
    slideIndex: 1,
    baseURL: {},
    init: function () {
        var getUrl = window.location;
        var baseUrl = getUrl.protocol + "//" + getUrl.host + "/";
        CommerceManager.baseURL = baseUrl;
        const elements = document.getElementsByClassName('commerce-item-container');
        Array.from(elements).forEach (element => this.initElements (element));
    },
    initElements (element){
        element.style.cursor = 'pointer';
        element.onclick = function () {
            let button = this;
            button.busy = true;
            let modal = document.getElementById("showModal");
            modal.style.display = "block";
        };
        element.onmouseover = function () {
            this.style.backgroundColor = '#ECECEC';
        };
        element.onmouseout = function () {
            this.style.backgroundColor = '';
        };
    },

    plusSlides:

        function (n) {
            CommerceManager.showSlides(CommerceManager.slideIndex += n);
        }

    ,
    currentSlide: function (n) {
        CommerceManager.showSlides(CommerceManager.slideIndex = n);
    }
    ,
    showSlides: function () {
        var i;
        let slides = document.getElementsByClassName("mySlides");
        let dots = document.getElementsByClassName("dot");
        if (CommerceManager.slideIndex > slides.length) {
            CommerceManager.slideIndex = 1
        }
        if (CommerceManager.slideIndex < 1) {
            CommerceManager.slideIndex = slides.length
        }
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(" active", "");
        }
        slides[CommerceManager.slideIndex - 1].style.display = "block";
        dots[CommerceManager.slideIndex - 1].className += " active";
    }
}