var ProductManager = {
	baseURL : {},
	selectedProduct: {},
	productTable: {},
	init : function(tableSelector) {
		var getUrl = window.location;
		var baseUrl = getUrl.protocol + "//" + getUrl.host + "/";
		ProductManager.baseURL = baseUrl;
		$("#add-new-product").click (function (e){
			e.preventDefault();
			var button = this;
			button.busy = true;
			ProductManager.saveNewProduct();
		});
		$("#cancelModalButton").click (function (e){
			e.preventDefault();
			var button = this;
			button.busy = true;
			document.getElementById("deleteModal").style.display = "none";
		});
		$("#cancelEditModalButton").click (function (e){
			e.preventDefault();
			var button = this;
			button.busy = true;
			document.getElementById("editModal").style.display = "none";
		});
		ProductManager.initProductTable (tableSelector);
	},
	deleteProduct: function (product) {
		console.log("Waiting...");
		return jQuery.ajax({
			type: "DELETE",
			url: ProductManager.baseURL + "rest/product",
			data: {
				id: product.id
			},
			dataType: "json",
			ContentType: "application/json",
			success: function (data, textStatus, jQxhr) {
			},
			error: function (jqXhr, textStatus, errorThrown) {
			}
		})
			.always(function (jqXhr, textStatus, errorThrown) {
				ProductManager.productTable.ajax.reload(null, false);
			});
	},
	saveNewProduct: function () {
		console.log("Waiting...");
		return jQuery.ajax({
			type: "POST",
			url: ProductManager.baseURL + "rest/product",
			data: {
				name: document.getElementById('name').value,
			},
			dataType: "json",
			ContentType: "application/json",
			success: function (data, textStatus, jQxhr) {
				console.log ("success");
			},
			error: function (jqXhr, textStatus, errorThrown) {
				console.log ("error");
			}
		})
			.always(function (jqXhr, textStatus, errorThrown) {
				ProductManager.productTable.ajax.reload(null, false);
			});
	},
	editProduct: function (id) {
		console.log("Waiting...");
		return jQuery.ajax({
			type: "PUT",
			url: ProductManager.baseURL + "rest/product",
			data: {
				id: id,
				name: document.getElementById('editName').value,
			},
			dataType: "json",
			ContentType: "application/json",
			success: function (data, textStatus, jQxhr) {
				console.log ("success");
			},
			error: function (jqXhr, textStatus, errorThrown) {
				console.log ("error");
			}
		})
			.always(function (jqXhr, textStatus, errorThrown) {
				ProductManager.productTable.ajax.reload(null, false);
			});
	},
	initProductTable (tableSelector){
		ProductManager.productTable=$(tableSelector).DataTable({
				"processing": true,
		        "serverSide": true,
		        "ajax": {
		            "url": ProductManager.baseURL+"datatable/products",
		            "type": "GET",
		            "dataSrc": function ( json ) {
						jQuery.ajaxSettings.traditional = true;
						return json.data;
					},
					"data": function ( d ) {
						jQuery.ajaxSettings.traditional = false;
					}
		        },
	        	columns: [
	        		{ "data": "id" },
	        		{ "data": "name" , "className": 'centered-column'},
			        { "data": null, "dafaultContent" : false , "className": 'centered-column'}
			     ],
				columnDefs: [
					{
						render: function ( data, type, row ) {
							return '<button class="titled delete-product-button" type="button" id="deleteProduct" title="Delete Product"><span class="fa fa-trash" aria-hidden="true"></span></button>';
						},
						targets: 2
					},
					{
						render: function ( data, type, row ) {
							return '<button class="titled edit-product-button" type="button" id="editProduct" title="Edit Product"><span class="fa fa-pencil" aria-hidden="true"></span></button>';
						},
						targets: 3
					},
					{ visible: false,  targets: [0] },
					{ orderable: false,  targets: [ 2, 3 ] }
				],
		        order: [[ 0, 'asc' ]]
	    	});
		$(tableSelector + " tbody").on ('click', '#deleteProduct', function() {
	    	var tableRow = ProductManager.productTable.row($(this).parents('tr') );
	   		var product = tableRow.data();
	   		ProductManager.selectedProduct = product;
	   		ProductManager.showDialog (product);
			});
		$(tableSelector + " tbody").on ('click', '#editProduct', function() {
	    	var tableRow = ProductManager.productTable.row($(this).parents('tr') );
	   		var product = tableRow.data();
	   		ProductManager.selectedProduct = product;
	   		ProductManager.showEditDialog (product);
			});
		},
		showDialog : function (product){
			var modal = document.getElementById("deleteModal");
			modal.style.display = "block";
			$("#deleteModalButton").click (function (e){
				e.preventDefault();
				var button = this;
				button.busy = true;
				ProductManager.deleteProduct (product);
				document.getElementById("deleteModal").style.display = "none";
			});
		},
		showEditDialog : function (product){
			var editModal = document.getElementById("editModal");
			$("#editName").val(product.name);
			
			editModal.style.display = "block";
			$("#edit-product").click (function (e){
				e.preventDefault();
				var button = this;
				button.busy = true;
				ProductManager.editProduct (product.id);
				document.getElementById("editModal").style.display = "none";
			});
		}
}