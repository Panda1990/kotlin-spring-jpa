var HomeManager = {
	baseURL : {},
	init : function() {
		var getUrl = window.location;
		var baseUrl = getUrl.protocol + "//" + getUrl.host + "/";
		HomeManager.baseURL = baseUrl;
		
		$("#jobHomeLink").click(function() {
			window.location = NavManager.baseURL + "jobs";
		});
		$("#productHomeLink").click(function() {
			window.location = NavManager.baseURL + "products";
		});
		$("#customerHomeLink").click(function() {
			window.location = NavManager.baseURL + "customers";
		});
	}
}