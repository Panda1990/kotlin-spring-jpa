var CustomerManager = {
	baseURL : {},
	customerTable : {},
	selectedCustomer : {},
	init : function(tableSelector) {
		var getUrl = window.location;
		var baseUrl = getUrl.protocol + "//" + getUrl.host + "/";
		CustomerManager.baseURL = baseUrl;
		$("#add-new-customer").click (function (e){
			e.preventDefault();
			var button = this;
			button.busy = true;
			CustomerManager.saveNewCustomer();
		});
		$("#cancelModalButton").click (function (e){
			e.preventDefault();
			var button = this;
			button.busy = true;
			document.getElementById("deleteModal").style.display = "none";
		});
		$("#cancelEditModalButton").click (function (e){
			e.preventDefault();
			var button = this;
			button.busy = true;
			document.getElementById("editModal").style.display = "none";
		});
		CustomerManager.initCustomerTable (tableSelector);
	},
	deleteCustomer: function (customer) {
		console.log("Waiting...");
		console.log(customer);
		return jQuery.ajax({
			type: "DELETE",
			url: CustomerManager.baseURL + "rest/customer",
			data: {
				id: customer.id
			},
			dataType: "json",
			ContentType: "application/json",
			success: function (data, textStatus, jQxhr) {
			},
			error: function (jqXhr, textStatus, errorThrown) {
			}
		})
			.always(function (jqXhr, textStatus, errorThrown) {
				CustomerManager.customerTable.ajax.reload(null, false);
			});
	},
	saveNewCustomer: function () {
		console.log("Waiting...");
		return jQuery.ajax({
			type: "POST",
			url: CustomerManager.baseURL + "rest/customer",
			data: {
				firstName: document.getElementById('firstName').value,
				lastName: document.getElementById('lastName').value,
				email: document.getElementById('email').value,
				telephone: document.getElementById('telephone').value,
			},
			dataType: "json",
			ContentType: "application/json",
			success: function (data, textStatus, jQxhr) {
			},
			error: function (jqXhr, textStatus, errorThrown) {
			}
		})
			.always(function (jqXhr, textStatus, errorThrown) {
				CustomerManager.customerTable.ajax.reload(null, false);
			});
	},
	editCustomer: function (id) {
		console.log("Waiting...");
		return jQuery.ajax({
			type: "PUT",
			url: CustomerManager.baseURL + "rest/customer",
			data: {
				id: id,
				firstName: document.getElementById('editFirstName').value,
				lastName: document.getElementById('editLastName').value,
				email: document.getElementById('editEmail').value,
				telephone: document.getElementById('editTelephone').value,
			},
			dataType: "json",
			ContentType: "application/json",
			success: function (data, textStatus, jQxhr) {
			},
			error: function (jqXhr, textStatus, errorThrown) {
			}
		})
			.always(function (jqXhr, textStatus, errorThrown) {
				CustomerManager.customerTable.ajax.reload(null, false);
			});
	},
	initCustomerTable (tableSelector){
		CustomerManager.customerTable=$(tableSelector).DataTable({
				"processing": true,
		        "serverSide": true,
		        "ajax": {
		            "url": CustomerManager.baseURL+"datatable/customers",
		            "type": "GET",
		            "dataSrc": function ( json ) {
						jQuery.ajaxSettings.traditional = true;
						return json.data;
					},
					"data": function ( d ) {
						jQuery.ajaxSettings.traditional = false;
					}
		        },
	        	columns: [
	        		{ "data": "id" },
	        		{ "data": "firstName" , "className": 'centered-column'},
			        { "data": "lastName" , "className": 'centered-column'},
			        { "data": "email" , "className": 'centered-column'},
			        { "data": "telephone" , "className": 'centered-column'},
			        { "data": null, "dafaultContent" : false , "className": 'centered-column'},
			        { "data": null, "dafaultContent" : false , "className": 'centered-column'}
			     ],
				columnDefs: [
					{
						render: function ( data, type, row ) {
							return '<button class="titled delete-customer-button" type="button" id="deleteCustomer" title="Delete Customer"><span class="fa fa-trash" aria-hidden="true"></span></button>';
						},
						targets: 5
					},
					{
						render: function ( data, type, row ) {
							return '<button class="titled edit-customer-button" type="button" id="editCustomer" title="Edit Customer"><span class="fa fa-pencil" aria-hidden="true"></span></button>';
						},
						targets: 6
					},
					{ visible: false,  targets: [0] },
					{ orderable: false,  targets: [ 5, 6] }
				],
		        order: [[ 0, 'asc' ]]
	    	});
		$(tableSelector + " tbody").on ('click', '#deleteCustomer', function() {
	    	var tableRow = CustomerManager.customerTable.row($(this).parents('tr') );
	   		var customer = tableRow.data();
	   		CustomerManager.selectedCustomer = customer;
	    	$(this).blur();
	   		CustomerManager.showDialog (customer);
			});
		$(tableSelector + " tbody").on ('click', '#editCustomer', function() {
	    	var tableRow = CustomerManager.customerTable.row($(this).parents('tr') );
	   		var customer = tableRow.data();
	   		CustomerManager.selectedCustomer = customer;
	    	$(this).blur();
	   		CustomerManager.showEditDialog (customer);
			});
		},
		showDialog : function (customer){
			var modal = document.getElementById("deleteModal");
			modal.style.display = "block";
			$("#deleteModalButton").click (function (e){
				e.preventDefault();
				var button = this;
				button.busy = true;
				CustomerManager.deleteCustomer (customer);
				document.getElementById("deleteModal").style.display = "none";
			});
		},
		showEditDialog : function (customer){
			var editModal = document.getElementById("editModal");
			$("#editFirstName").val(customer.firstName);
			$("#editLastName").val(customer.lastName);
			$("#editEmail").val(customer.email);
			$("#editTelephone").val(customer.telephone);
			
			editModal.style.display = "block";
			$("#editModalButton").click (function (e){
				e.preventDefault();
				var button = this;
				button.busy = true;
				CustomerManager.editCustomer (customer.id);
				document.getElementById("editModal").style.display = "none";
			});
		}
}