package com.example.spring.kotlin.demo.repository.entities.datatable

import java.lang.Boolean
import java.util.*
import java.util.regex.Pattern
import javax.servlet.http.HttpServletRequest


/**
 * The Class DataTableRequest.
 *
 * @author pavan.solapure
 */
class DataTableRequest(request: HttpServletRequest) {

    /** The unique id.  */
    var uniqueId: String? = null
    /** The draw.  */
    var draw: String? = null
    /** The start.  */
    var start: Int? = null
    /** The length.  */
    var length: Int? = null
    /** The search.  */
    var search: String? = null
    /** The regex.  */
    var isRegex = false
    /** The columns.  */
    var columns: List<DataTableColumnSpecs>? = null
    /** The order.  */
    var order: DataTableColumnSpecs? = null
    /** The is global search.  */
    var isGlobalSearch = false

    /**
     * Prepare data table request.
     *
     * @param request the request
     */
    private fun prepareDataTableRequest(request: HttpServletRequest) {
        val parameterNames = request.parameterNames
        if (parameterNames.hasMoreElements()) {
            start = request.getParameter(PaginationCriteria.PAGE_NO).toInt()
            length = request.getParameter(PaginationCriteria.PAGE_SIZE).toInt()
            uniqueId = request.getParameter("_")
            draw = request.getParameter(PaginationCriteria.DRAW)
            search = request.getParameter("search[value]")
            isRegex = Boolean.valueOf(request.getParameter("search[regex]"))
            val ordering = request.getParameter("order[0][column]")
            //Setting ordering initially to -1
            var sortableCol = -1
            if (ordering != null) {
                sortableCol = request.getParameter("order[0][column]").toInt()
            }
            val columns: MutableList<DataTableColumnSpecs> = ArrayList()
            if (!DataTableUtils.isObjectEmpty(search)) {
                this.isGlobalSearch = true
            }
            maxParamsToCheck = getNumberOfColumns(request)
            for (i in 0 until maxParamsToCheck) {
                if (null != request.getParameter("columns[$i][data]") && !"null".equals(request.getParameter("columns[$i][data]"), ignoreCase = true)
                        && !DataTableUtils.isObjectEmpty(request.getParameter("columns[$i][data]"))) {
                    val colSpec: DataTableColumnSpecs = DataTableUtils.prepareColumnSpecs(request, i)
                    if (i == sortableCol) {
                        order = colSpec
                    }
                    columns.add(colSpec)
                    if (!DataTableUtils.isObjectEmpty(colSpec.search)) {
                        this.isGlobalSearch = false
                    }
                }
            }
            if (!DataTableUtils.isObjectEmpty(columns)) {
                this.columns = columns
            }
        }
    }

    private fun getNumberOfColumns(request: HttpServletRequest): Int {
        val p = Pattern.compile("columns\\[[0-9]+\\]\\[data\\]")
        val params: Enumeration<*> = request.parameterNames
        val lstOfParams: MutableList<String> = ArrayList()
        while (params.hasMoreElements()) {
            val paramName = params.nextElement() as String
            val m = p.matcher(paramName)
            if (m.matches()) {
                lstOfParams.add(paramName)
            }
        }
        return lstOfParams.size
    }

    /**
     * Gets the pagination request.
     *
     * @return the pagination request
     */
    val paginationRequest: PaginationCriteria
        get() {
            val pagination = PaginationCriteria()
            pagination.start = start
            pagination.pageSize = length
            var sortBy: SortBy? = null
            if (!DataTableUtils.isObjectEmpty(order)) {
                sortBy = SortBy()
                sortBy.addSort(order!!.data!!, SortOrder.fromValue(order!!.sortDir!!))
            }
            val filterBy = FilterBy()
            filterBy.isGlobalSearch = isGlobalSearch
            if (isGlobalSearch) {
                filterBy.globalFilter = search
            }
            for (colSpec in columns!!) {
                if (colSpec.isSearchable) {
                    if (!DataTableUtils.isObjectEmpty(search)
                            || !DataTableUtils.isObjectEmpty(colSpec.search)) {
                        filterBy.addFilter(colSpec.data!!,
                                if (isGlobalSearch) search!! else colSpec.search!!)
                    }
                }
            }
            pagination.sortBy  = sortBy
            pagination.filterBy = filterBy
            return pagination
        }

    /** The max params to check.  */
    var maxParamsToCheck = 0

    /**
     * Instantiates a new data table request.
     *
     * @param request the request
     */
    init {
        prepareDataTableRequest(request)
    }
}