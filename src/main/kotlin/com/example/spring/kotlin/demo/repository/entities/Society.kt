package com.example.spring.kotlin.demo.repository.entities

import org.json.JSONObject
import javax.persistence.*

@Entity
class Society {
    override fun toString(): String {
        return "Society [CF=" + cf + ", customers=" + customers + ", products=" + products + "]"
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var cf: Long? = null
    @Column(name = "society_name", unique = true)
    var name: String? = null
    @ElementCollection
    var customers = mutableListOf<Customer>()
    @ElementCollection
    var products = mutableListOf<Product>()

    val cusomers: List<Customer>?
        get() = customers

    fun addProduct(product: Product): Boolean {
        return products.add(product)
    }

    fun addCustomer(customer: Customer): Boolean {
        return customers.add(customer)
    }

    fun deleteProduct(product: Product): Boolean {
        return products.remove(product)
    }

    fun deleteCustomer(customer: Customer): Boolean {
        return customers.remove(customer)
    }

    fun addProducts(products: List<Product>?): Boolean {
        return this.products.addAll(products!!)
    }

    fun addCustomers(customers: List<Customer>?): Boolean {
        return this.customers.addAll(customers!!)
    }

    constructor(name: String?, customers: MutableList<Customer>, products: MutableList<Product>) : super() {
        this.name = name
        this.customers = customers
        this.products = products
    }

    constructor() : super() { // TODO Auto-generated constructor stub
    }

    constructor(json: JSONObject) : super() {
        println(json)
        val arrayCustomer = json.getJSONArray("customers")
        println(arrayCustomer.length())
        arrayCustomer.forEach { customer: Any ->
            val jsonObject = customer as JSONObject
            customers.add(Customer(jsonObject.getString("firstName"), jsonObject.getString("lastName"), jsonObject.getString("email"), jsonObject.getString("telephone")))
        }
        val arrayProduct = json.getJSONArray("products")
        arrayProduct.forEach { product: Any ->
            val jsonObjectProduct = product as JSONObject
            products.add(Product(jsonObjectProduct.getString("name")))
        }
    }

    fun copyOf(society: Society) {
        name = society.name
        customers = society.customers
        products = society.products
    }

}
