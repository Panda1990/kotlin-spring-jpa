package com.example.spring.kotlin.demo.repository.controller

import com.example.spring.kotlin.demo.repository.entities.Product
import com.example.spring.kotlin.demo.repository.service.ProductService
import org.springframework.context.annotation.Lazy
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.util.MultiValueMap
import org.springframework.web.bind.annotation.*


@RestController
@RequestMapping("/rest/product")
class ProductController (@Lazy private val productService: ProductService) {

    @GetMapping
    fun getAllProducts(@RequestParam(defaultValue = "0") pageNum: Int?,
                       @RequestParam(defaultValue = "10") pageSize: Int?, @RequestParam(defaultValue = "id") sortBy: String?): ResponseEntity<List<Product?>> {
        val list= productService.findProductsPaginated(pageNum, pageSize, sortBy, "ASC", null)
        return ResponseEntity(list, HttpHeaders(), HttpStatus.OK)
    }

    @PostMapping
    fun saveProduct(@RequestBody map: MultiValueMap<String?, String?>): ResponseEntity<Product> {
        val product: Product? = null
        try {
            productService.save(Product(map["name"]!![0]))
        } catch (e: Exception) {
            println("exception")
            return ResponseEntity(HttpStatus.NOT_ACCEPTABLE)
        }
        println("Success")
        return ResponseEntity(product, HttpStatus.OK)
    }

    @DeleteMapping
    fun deleteProduct(@RequestParam id: Long?): ResponseEntity<String> {
        try {
            productService.deleteProduct(id!!)
        } catch (e: Exception) {
            return ResponseEntity("Product Not Present", HttpStatus.NOT_FOUND)
        }
        return ResponseEntity("Deleted Successfully", HttpStatus.OK)
    }

    @PutMapping
    fun editProduct(@RequestParam map: MultiValueMap<String?, String?>): ResponseEntity<Product> {
        val product = Product(map["name"]!![0])
        val id = map["id"]!![0]!!.toLong()
        product.id = id
        var productTmp: Product? = null
        try {
            productTmp = productService.editProduct(product)
        } catch (e: Exception) {
            return ResponseEntity(productTmp, HttpStatus.NOT_FOUND)
        }
        return ResponseEntity(productTmp, HttpStatus.OK)
    }
}
