package com.example.spring.kotlin.demo.repository.service

import com.example.spring.kotlin.demo.repository.JobRepository
import com.example.spring.kotlin.demo.repository.entities.Job
import com.example.spring.kotlin.demo.repository.entities.datatable.DataTablePage
import com.example.spring.kotlin.demo.repository.entities.datatable.FilterBy
import org.springframework.context.annotation.Lazy
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.data.util.Streamable
import org.springframework.stereotype.Service
import java.util.*


@Service
class JobService (@Lazy private val jobRepository: JobRepository) {

    fun findJobPaginated(pageNum: Int?, pageSize: Int?, sortBy: String?, ordering: String?,
                         search: String?): List<Job?> {
        val paging: Pageable = PageRequest.of(pageNum!!, pageSize!!, Sort.by(Sort.Direction.fromString(ordering!!), sortBy))
        if (null == search) {
            val pageResult = jobRepository.findAll(paging)
            return if (pageResult.hasContent()) pageResult.content else ArrayList<Job>()
        }
        val jobs: MutableList<Job?> = LinkedList()
        jobRepository.findAll(paging).forEach{
            if (it?.customer!!.firstName!!.toLowerCase().contains(search.toLowerCase())
                    || it.customer!!.lastName!!.toLowerCase().contains(search.toLowerCase())
                    || it.product!!.name!!.toLowerCase().contains(search.toLowerCase())
                    || it.description!!.toLowerCase().contains(search.toLowerCase())) {
                jobs.add(it)
            }
        }
        return jobs
    }

    fun findJobByCustomer(name: String, pageNum: Int?, pageSize: Int?, sortBy: String?): List<Job> {
        val paging: Pageable = PageRequest.of(pageNum!!, pageSize!!, Sort.by(sortBy))
        val jobs: Streamable<Job?> = jobRepository.findAll(paging).filter{
            (it?.customer!!.firstName!!.toLowerCase().contains(name.toLowerCase())
                    || it.customer!!.lastName!!.toLowerCase().contains(name.toLowerCase()))
        }
        return jobs.toList() as List<Job>
    }

    fun findJobByProduct(name: String, pageNum: Int?, pageSize: Int?, sortBy: String?): List<Job> {
        val paging: Pageable = PageRequest.of(pageNum!!, pageSize!!, Sort.by(sortBy))
        val jobs: Streamable<Job?> = jobRepository.findAll(paging).filter{
            it?.product!!.name!!.toLowerCase().contains(name.toLowerCase())
        }
        return jobs.toList() as List<Job>
    }

    fun getJobsPaginated(filterBy: FilterBy, start: Int, pageSize: Int, orderClause: String?,
                         orderDirection: String?): DataTablePage<Job?> {
        var start = start
        val page = DataTablePage<Job?>()
        val totalLong = jobRepository.count()
        start = start / pageSize
        page.data = findJobPaginated(start, pageSize, orderClause, orderDirection, filterBy.globalFilter)
        val totalFiltered = if (filterBy.isGlobalSearch) page.data!!.size else totalLong.toInt()
        page.recordsTotal = totalLong.toInt()
        page.recordsFiltered = totalFiltered
        return page
    }

    fun save(job: Job): Job {
        return jobRepository.save(job)
    }

    fun deleteJob(id: Long) {
        jobRepository.deleteById(id)
    }

    fun editJob(job: Job): Job? {
        val jobDB = jobRepository.findById(job.id!!)
        if (jobDB.isPresent) {
            System.out.println(job)
            val jobTmp = jobDB.get()
            jobTmp.completing = job.completing
            jobTmp.customer=job.customer
            jobTmp.description = job.description
            jobTmp.pickUpDate = job.pickUpDate
            jobTmp.price = job.price
            jobTmp.product = job.product
            jobTmp.startJobDate = job.startJobDate
            jobRepository.save(jobTmp)
            return jobTmp
        }
        return null
    }
}
