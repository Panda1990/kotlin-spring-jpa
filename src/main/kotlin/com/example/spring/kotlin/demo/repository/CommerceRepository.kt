package com.example.spring.kotlin.demo.repository

import com.example.spring.kotlin.demo.repository.entities.CommerceItem
import com.example.spring.kotlin.demo.repository.entities.Customer
import org.springframework.data.repository.PagingAndSortingRepository

interface CommerceRepository : PagingAndSortingRepository<CommerceItem, Long>{
    fun findCommerceItemsByDescription(description: String): List<CommerceItem>
    fun findCommerceItemsByName(firstName: String): List<CommerceItem>
    /*fun findCommerceItemsByNameOrDescription(search: String): List<CommerceItem>*/
}