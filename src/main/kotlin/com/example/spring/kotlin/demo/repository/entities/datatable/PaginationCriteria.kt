package com.example.spring.kotlin.demo.repository.entities.datatable


/**
 * The Class PaginationCriteria.
 */
class PaginationCriteria {
    /**
     * Gets the page number.
     *
     * @return the pageNumber
     */
    /**
     * Sets the page number.
     *
     * @param pageNumber the pageNumber to set
     */
    /** The page number.  */
    var start: Int? = null
        get() = if (null == field) 0 else field
    /**
     * Gets the page size.
     *
     * @return the pageSize
     */
    /**
     * Sets the page size.
     *
     * @param pageSize the pageSize to set
     */
    /** The page size.  */
    var pageSize: Int? = null
        get() = if (null == field) 10 else field
    /**
     * Gets the total records.
     *
     * @return the totalRecords
     */
    /**
     * Sets the total records.
     *
     * @param totalRecords the totalRecords to set
     */
    /** The total records.  */
    var totalRecords: Int? = null
    /**
     * Gets the sort by.
     *
     * @return the sortBy
     */
    /**
     * Sets the sort by.
     *
     * @param sortBy the sortBy to set
     */
    /** The sort by.  */
    var sortBy: SortBy? = null
    /**
     * Gets the filter by.
     *
     * @return the filterBy
     */
    /**
     * Sets the filter by.
     *
     * @param filterBy the filterBy to set
     */
    /** The filter by.  */
    var filterBy: FilterBy? = null

    /**
     * Checks if is filter by empty.
     *
     * @return true, if is filter by empty
     */
    val isFilterByEmpty: Boolean
        get() = if (null == filterBy || null == filterBy!!.getMapOfFilters() || filterBy!!.getMapOfFilters()!!.size == 0) {
            true
        } else false

    /**
     * Checks if is sort by empty.
     *
     * @return true, if is sort by empty
     */
    val isSortByEmpty: Boolean
        get() = if (null == sortBy || null == sortBy!!.sortBys || sortBy!!.sortBys!!.size === 0) {
            true
        } else false

    /**
     * Gets the filter by clause.
     *
     * @return the filter by clause
     */
    val filterByClause: String
        get() {
            var fbsb: StringBuilder? = null
            if (!isFilterByEmpty) {
                val fbit = filterBy!!.getMapOfFilters()!!.entries.iterator()
                while (fbit.hasNext()) {
                    val pair = fbit.next()
                    if (null == fbsb) {
                        fbsb = StringBuilder()
                        fbsb.append(BRKT_OPN)
                        fbsb.append(SPACE)
                                .append(BRKT_OPN)
                                .append(pair.key)
                                .append(LIKE_PREFIX)
                                .append(pair.value)
                                .append(LIKE_SUFFIX)
                                .append(BRKT_CLS)
                    } else {
                        fbsb.append(if (filterBy!!.isGlobalSearch) OR else AND)
                                .append(BRKT_OPN)
                                .append(pair.key)
                                .append(LIKE_PREFIX)
                                .append(pair.value)
                                .append(LIKE_SUFFIX)
                                .append(BRKT_CLS)
                    }
                }
                fbsb!!.append(BRKT_CLS)
            }
            return fbsb?.toString() ?: BLANK
        }

    /**
     * Gets the order by clause.
     *
     * @return the order by clause
     */
    val orderByClause: String
        get() {
            var sbsb: StringBuilder? = null
            if (!isSortByEmpty) {
                val sbit: Iterator<Map.Entry<String, SortOrder>> = sortBy?.sortBys?.entries?.iterator()!!
                while (sbit.hasNext()) {
                    val pair = sbit.next()
                    if (null == sbsb) {
                        sbsb = StringBuilder()
                        sbsb.append(ORDER_BY).append(pair.key).append(SPACE).append(pair.value)
                    } else {
                        sbsb.append(COMMA).append(pair.key).append(SPACE).append(pair.value)
                    }
                }
            }
            return sbsb?.toString() ?: BLANK
        }

    companion object {
        /** The Constant BLANK.  */
        private const val BLANK = ""
        /** The Constant SPACE.  */
        private const val SPACE = " "
        /** The Constant LIKE_PREFIX.  */
        private const val LIKE_PREFIX = " LIKE '%"
        /** The Constant LIKE_SUFFIX.  */
        private const val LIKE_SUFFIX = "%' "
        /** The Constant AND.  */
        private const val AND = " AND "
        /** The Constant OR.  */
        private const val OR = " OR "
        /** The Constant ORDER_BY.  */
        private const val ORDER_BY = " ORDER BY "
        private const val BRKT_OPN = " ( "
        private const val BRKT_CLS = " ) "
        /** The Constant COMMA.  */
        private const val COMMA = " , "
        /** The Constant PAGE_NO.  */
        const val PAGE_NO = "start"
        /** The Constant PAGE_SIZE.  */
        const val PAGE_SIZE = "length"
        /** The Constant DRAW.  */
        const val DRAW = "draw"
    }
}