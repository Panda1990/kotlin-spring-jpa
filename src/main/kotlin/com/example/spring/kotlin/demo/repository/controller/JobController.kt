package com.example.spring.kotlin.demo.repository.controller

import com.example.spring.kotlin.demo.repository.entities.Job
import com.example.spring.kotlin.demo.repository.service.CustomerService
import com.example.spring.kotlin.demo.repository.service.JobService
import com.example.spring.kotlin.demo.repository.service.ProductService
import org.springframework.context.annotation.Lazy
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.util.MultiValueMap
import org.springframework.web.bind.annotation.*
import java.sql.Date


@RestController
@RequestMapping("/rest/job")
class JobController (@Lazy private val jobService: JobService, @Lazy private val customerService: CustomerService, @Lazy private val productService: ProductService){

    @PostMapping
    fun saveJob(@RequestBody parameters: MultiValueMap<String?, String?>): ResponseEntity<Job>? {
        println(parameters["customerID"])
        println(parameters["productID"])
        println(parameters["price"])
        println(parameters["startDate"])
        println(parameters["pickUpDate"])
        println(parameters["description"])
        val customer = customerService.findByID(parameters["customerID"]!![0]!!.toLong())
        if (!customer?.isPresent()!!) return null
        val product= productService.findByID(parameters["productID"]!![0]!!.toLong())
        if (!product?.isPresent()!!) return null
        var job: Job? = null
        try {
            job = jobService
                    .save(Job(customer.get(), product.get(), parameters["price"]!![0]!!.toDouble(),
                            Date.valueOf(parameters["startDate"]!![0]),
                            Date.valueOf(parameters["pickUpDate"]!![0]), parameters["description"]!![0]))
        } catch (e: Exception) {
            return ResponseEntity<Job>(HttpStatus.NOT_ACCEPTABLE)
        }
        return ResponseEntity<Job>(job, HttpStatus.OK)
    }

    @DeleteMapping
    fun deleteJob(@RequestParam id: Long?): ResponseEntity<String> {
        try {
            jobService.deleteJob(id!!)
        } catch (e: Exception) {
            return ResponseEntity("Job Not Present", HttpStatus.NOT_FOUND)
        }
        println("Success")
        return ResponseEntity("Deleted Successfully", HttpStatus.OK)
    }

    @PutMapping
    fun editJob(@RequestParam parameters: MultiValueMap<String?, String?>): ResponseEntity<Job>? {
        val customer= customerService.findByID(parameters["customerID"]!![0]!!.toLong())
        if (!customer!!.isPresent()) return null
        val product= productService.findByID(parameters["productID"]!![0]!!.toLong())
        if (!product!!.isPresent()) return null
        val job = Job(customer.get(), product.get(), parameters["price"]!![0]!!.toDouble(),
                Date.valueOf(parameters["startDate"]!![0]),
                Date.valueOf(parameters["pickUpDate"]!![0]), parameters["description"]!![0])
        val id = parameters["id"]!![0]!!.toLong()
        job.id = id
        System.out.println(job)
        var jobTmp: Job? = null
        try {
            jobTmp = jobService.editJob(job)
        } catch (e: Exception) {
            return ResponseEntity<Job>(jobTmp, HttpStatus.NOT_FOUND)
        }
        return ResponseEntity<Job>(jobTmp, HttpStatus.OK)
    }
}
