package com.example.spring.kotlin.demo.repository.entities

import javax.persistence.*

@Entity
class CommerceItem {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Long? = null
    @Column
    var name: String? = null
    @Column
    var description: String? = null
    @Column
    var price: Float? = null
    @Column
    var coverImage: String? = null
    @ElementCollection
    var images: List<String> = mutableListOf()

    constructor() : super() { // TODO Auto-generated constructor stub
    }

    constructor(name: String?, description: String?, price: Float?, coverImage: String?, images: List<String>) {
        this.name = name
        this.description = description
        this.price = price
        this.coverImage = coverImage
        this.images = images
    }

    companion object {
        /**
         *
         */
        const val serialversionuid = -6330899619667053818L

    }
}