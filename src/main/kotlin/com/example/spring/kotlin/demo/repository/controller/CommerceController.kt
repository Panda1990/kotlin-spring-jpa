package com.example.spring.kotlin.demo.repository.controller

import com.example.spring.kotlin.demo.repository.entities.CommerceItem
import com.example.spring.kotlin.demo.repository.service.CommerceService
import org.springframework.context.annotation.Lazy
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/rest/commerce")
class CommerceController (@Lazy private val commerceService: CommerceService) {
    @GetMapping
    fun getCommerceItems(@RequestParam(defaultValue = "0") pageNum: Int?,
                       @RequestParam(defaultValue = "10") pageSize: Int?, @RequestParam(defaultValue = "id") sortBy: String?): ResponseEntity<List<CommerceItem?>> {
        val list= commerceService.findCommerceItemPaginated(pageNum, pageSize, sortBy, "ASC", null)
        return ResponseEntity(list, HttpHeaders(), HttpStatus.OK)
    }
}