package com.example.spring.kotlin.demo.repository.controller

import com.example.spring.kotlin.demo.repository.entities.Customer
import com.example.spring.kotlin.demo.repository.service.CustomerService
import org.springframework.context.annotation.Lazy
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.util.MultiValueMap
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.ModelAndView


@Controller
@RequestMapping("/rest/customer")
class CustomerController (@Lazy private val customerService: CustomerService){

    @GetMapping
    fun getAllCustomers(@RequestParam(defaultValue = "0") pageNum: Int?,
                        @RequestParam(defaultValue = "10") pageSize: Int?, @RequestParam(defaultValue = "id") sortBy: String?): ResponseEntity<List<Customer?>> {
        val list = customerService.findCustomersPaginated(pageNum, pageSize, sortBy, "ASC", null)
        return ResponseEntity(list, HttpHeaders(), HttpStatus.OK)
    }

    @GetMapping(path = ["/display"])
    fun displayCustomer(model: MutableMap<String?, Any?>): ModelAndView {
        val pageNum = 0
        val pageSize = 10
        val sortBy = "id"
        val list = customerService.findCustomersPaginated(pageNum, pageSize, sortBy, "ASC", null)
        model["customer"] = list
        return ModelAndView("index", model)
    }

    @PostMapping
    fun saveCustomer(@RequestBody map: MultiValueMap<String?, String?>): ResponseEntity<Customer> {
        println(map["firstName"]!![0])
        println(map["lastName"]!![0])
        println(map["email"]!![0])
        println(map["telephone"]!![0])
        val customer: Customer? = null
        try {
            customerService.saveCustomer(Customer(map["firstName"]!![0], map["lastName"]!![0],
                    map["email"]!![0], map["telephone"]!![0]))
        } catch (e: Exception) {
            return ResponseEntity(HttpStatus.NOT_ACCEPTABLE)
        }
        return ResponseEntity(customer, HttpStatus.OK)
    }

    @DeleteMapping
    fun deleteCustomer(@RequestParam id: Long?): ResponseEntity<String> {
        try {
            customerService.deleteCustomerByID(id!!)
        } catch (e: Exception) {
            return ResponseEntity("Customer Not Present", HttpStatus.NOT_FOUND)
        }
        return ResponseEntity("Deleted Successfully", HttpStatus.OK)
    }

    @PutMapping
    fun editCustomer(@RequestParam map: MultiValueMap<String?, String?>): ResponseEntity<Customer> {
        val customer = Customer(map["firstName"]!![0], map["lastName"]!![0],
                map["email"]!![0], map["telephone"]!![0])
        val id = map["id"]!![0]!!.toLong()
        customer.id = id
        println(customer)
        var customerTmp: Customer? = null
        try {
            customerTmp = customerService.editCustomer(customer)
        } catch (e: Exception) {
            return ResponseEntity(customerTmp, HttpStatus.NOT_FOUND)
        }
        return ResponseEntity(customerTmp, HttpStatus.OK)
    }
}
