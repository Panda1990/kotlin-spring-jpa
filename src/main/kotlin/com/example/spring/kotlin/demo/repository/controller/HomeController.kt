package com.example.spring.kotlin.demo.repository.controller

import com.example.spring.kotlin.demo.repository.entities.CommerceItem
import com.example.spring.kotlin.demo.repository.service.CommerceService
import com.example.spring.kotlin.demo.repository.service.CustomerService
import com.example.spring.kotlin.demo.repository.service.JobService
import com.example.spring.kotlin.demo.repository.service.ProductService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Lazy
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.servlet.ModelAndView


@RestController
class HomeController(@Lazy private val customerService: CustomerService, @Lazy private val productService: ProductService, @Lazy private val jobService: JobService, @Lazy private val commerceService: CommerceService) {
    @Value("\${spring.application.name}")
    lateinit var appName: String

    @GetMapping("/")
    fun homePage(model: MutableMap<String?, Any?>): ModelAndView {
        model["appName"] = appName
        //model["customers"] = customerService.findCustomersPaginated(0, 100, "lastName", "ASC", null)
        //model["products"] = productService.findProductsPaginated(0, 100, "name", "ASC", null)
        //model.put("jobs", jobService.getAllJob()));
        return ModelAndView("home", model)
    }

    @GetMapping("/jobs")
    fun jobs(model: MutableMap<String?, Any?>): ModelAndView {
        model["appName"] = appName
        //model["customers"] = customerService.findCustomersPaginated(0, 1000, "lastName", "ASC", null)
        //model["products"] = productService.findProductsPaginated(0, 1000, "name", "ASC", null)
        //model.put("jobs", jobService));
        return ModelAndView("jobs", model)
    }

    @GetMapping("/customers")
    fun customers(model: MutableMap<String?, Any?>): ModelAndView {
        model["appName"] = appName
        //model.put("customers", customerService.getAllCustomer(0, 100, "lastName"));
        return ModelAndView("customers", model)
    }

    @GetMapping("/products")
    fun products(model: MutableMap<String?, Any?>): ModelAndView {
        model["appName"] = appName
        //model.put("products", productService.getAllProducts(0, 100, "name"));
        return ModelAndView("products", model)
    }

    @GetMapping("/commerce")
    fun commerce(model: MutableMap<String?, Any?>): ModelAndView {
        model["appName"] = appName
        commerceService.saveCommerceItem(CommerceItem().apply {
            this.description = "ciao"
            val images = mutableListOf("05.jpg", "06.jpg", "07.jpg")
            this.images = images
            this.coverImage = "05.jpg"
            this.price = 10f
            this.name = "orecchini"
        })
        model["commerceItems"] = commerceService.findCommerceItemPaginated(0, 15, "id", "asc", null)
        println(model["commerceItems"])
        //model.put("products", productService.getAllProducts(0, 100, "name"));
        return ModelAndView("commerce", model)
    }
}
