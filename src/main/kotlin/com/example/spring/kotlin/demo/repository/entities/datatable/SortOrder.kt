package com.example.spring.kotlin.demo.repository.entities.datatable

/**
 * The Enum SortOrder.
 */
enum class SortOrder
/**
 * Instantiates a new sort order.
 *
 * @param v
 * the v
 */(
        /** The value.  */
        private val value: String) {
    /** The asc.  */
    ASC("ASC"),
    /** The desc.  */
    DESC("DESC");

    /**
     * Value.
     *
     * @return the string
     */
    fun value(): String {
        return value
    }

    companion object {
        /**
         * From value.
         *
         * @param v
         * the v
         * @return the sort order
         */
        fun fromValue(v: String): SortOrder {
            for (c in values()) {
                if (c.name.toLowerCase() == v.toLowerCase()) {
                    return c
                }
            }
            throw IllegalArgumentException(v)
        }
    }

}