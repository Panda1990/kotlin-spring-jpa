package com.example.spring.kotlin.demo.repository

import com.example.spring.kotlin.demo.repository.entities.Product

import org.springframework.data.repository.PagingAndSortingRepository
import java.util.*


interface ProductRepository : PagingAndSortingRepository<Product, Long> {
    fun findByName(name: String?): Optional<Product?>?
}
