package com.example.spring.kotlin.demo.repository.entities.datatable


/**
 * The Class SortBy.
 *
 * @author pavan.solapure
 */
class FilterBy {
    /** The map of sorts.  */
    private var mapOfFilters: MutableMap<String, String>? = null
    /**
     * Checks if is global search.
     *
     * @return the globalSearch
     */
    /**
     * Sets the global search.
     *
     * @param globalSearch the globalSearch to set
     */
    /** The global search.  */
    var isGlobalSearch = false
    var globalFilter: String? = null

    /**
     * Gets the map of filters.
     *
     * @return the mapOfFilters
     */
    fun getMapOfFilters(): Map<String, String>? {
        return mapOfFilters
    }

    /**
     * Sets the map of filters.
     *
     * @param mapOfFilters            the mapOfFilters to set
     */
    fun setMapOfFilters(mapOfFilters: MutableMap<String, String>?) {
        this.mapOfFilters = mapOfFilters
    }

    /**
     * Adds the sort.
     *
     * @param filterColumn the filter column
     * @param filterValue the filter value
     */
    fun addFilter(filterColumn: String, filterValue: String) {
        mapOfFilters!![filterColumn] = filterValue
    }

    /**
     * Instantiates a new sort by.
     */
    init {
        if (null == mapOfFilters) {
            mapOfFilters = HashMap()
        }
    }
}
