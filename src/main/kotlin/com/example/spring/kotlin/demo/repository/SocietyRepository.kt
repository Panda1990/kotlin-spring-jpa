package com.example.spring.kotlin.demo.repository

import com.example.spring.kotlin.demo.repository.entities.Society

import org.springframework.data.repository.PagingAndSortingRepository
import java.util.*


interface SocietyRepository : PagingAndSortingRepository<Society, Long> {
    fun findByName(name: String?): Optional<Society?>?
}
