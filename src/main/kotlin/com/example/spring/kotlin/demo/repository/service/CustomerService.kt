package com.example.spring.kotlin.demo.repository.service

import com.example.spring.kotlin.demo.repository.CustomerRepository
import com.example.spring.kotlin.demo.repository.entities.Customer
import com.example.spring.kotlin.demo.repository.entities.datatable.DataTablePage
import com.example.spring.kotlin.demo.repository.entities.datatable.FilterBy
import org.springframework.context.annotation.Lazy
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.data.util.Streamable
import org.springframework.stereotype.Service
import org.springframework.web.bind.annotation.RequestBody
import java.util.*
import kotlin.collections.ArrayList


@Service
class CustomerService (@Lazy private val customerRepository: CustomerRepository) {

    // Save Customer
    fun saveCustomer(@RequestBody customer: Customer): Customer {
        return customerRepository.save(customer)
    }

    fun findByFirstAndLastName(firstName: String, lastName: String): List<Customer> {
        val customers: MutableList<Customer> = mutableListOf()
        customerRepository.findAll().forEach {
            if (it?.firstName == firstName && it.lastName == lastName) {
                customers.add(it)
            }
        }
        return customers
    }

    // Find by First name
    fun findByFirstName(firstname: String?): List<Customer?>? {
        return customerRepository.findByFirstName(firstname)
    }

    // Find by Last Name
    fun findByLastName(lastName: String?): List<Customer?>? {
        return customerRepository.findByLastName(lastName)
    }

    // Find By Last Name or First Name paginated
    fun findByFirstAndLastNamePaginated(toSearch: String, page: Int?, pageSize: Int?,
                                        sortBy: String?): List<Customer> {
        val paging: Pageable = PageRequest.of(page!!, pageSize!!, Sort.by(sortBy))
        val pageResult: Streamable<Customer?> = customerRepository.findAll(paging).filter{
            (it?.firstName!!.toLowerCase().contains(toSearch.toLowerCase())
                    || it.lastName!!.toLowerCase().contains(toSearch.toLowerCase()))
        }
        return pageResult.toList() as List<Customer>
    }

    // Delete Customers using FirstName
    fun deleteCustomerByName(firstName: String?) {
        val customer = findByFirstName(firstName)
        customerRepository.deleteAll(customer!!)
    }

    // Delete Customers using Last Name
    fun deleteCustoemrsByLastName(lastName: String?) {
        val customers = findByLastName(lastName)
        customerRepository.deleteAll(customers!!)
    }

    // Delete Customer using ID
    fun deleteCustomerByID(ID: Long) {
        customerRepository.deleteById(ID)
    }

    fun findByID(ID: Long): Optional<Customer?> {
        return customerRepository.findById(ID)
    }

    val allCustomer: List<Customer>
        get() {
            val customers: MutableList<Customer> = mutableListOf()
            customerRepository.findAll().forEach{
                customers.add(it!!)
            }
            return customers
        }

    fun getCustomersPaginated(filterBy: FilterBy, start: Int, pageSize: Int,
                              orderClause: String?, orderDirection: String?): DataTablePage<Customer?> {
        var start = start
        val page = DataTablePage<Customer?>()
        val totalLong = customerRepository.count()
        start = start / pageSize
        page.data = findCustomersPaginated(start, pageSize, orderClause, orderDirection, filterBy.globalFilter)
        val totalFiltered = if (filterBy.isGlobalSearch) page.data!!.size else totalLong.toInt()
        page.recordsTotal = totalLong.toInt()
        page.recordsFiltered = totalFiltered
        return page
    }

    fun findCustomersPaginated(start: Int?, pageSize: Int?, orderClause: String?,
                               orderDirection: String?, search: String?): List<Customer?> {
        val paging: Pageable = PageRequest.of(start!!, pageSize!!, Sort.by(Sort.Direction.fromString(orderDirection!!), orderClause))
        if (null == search) {
            val pageResult = customerRepository.findAll(paging)
            return if (pageResult.hasContent()) pageResult.content else ArrayList<Customer>()
        }
        val customers: MutableList<Customer?> = LinkedList()
        customerRepository.findAll(paging).forEach{
            if (it?.firstName!!.toLowerCase().contains(search.toLowerCase())
                    || it.lastName!!.toLowerCase().contains(search.toLowerCase())
                    || it.email!!.toLowerCase().contains(search.toLowerCase())) {
                customers.add(it)
            }
        }
        return customers
    }

    fun editCustomer(customer: Customer): Customer? {
        val customerDB = customerRepository.findById(customer.id!!)
        var customerTmp: Customer? = null
        println(customer)
        if (customerDB.isPresent) {
            customerTmp = customerDB.get()
            println(customerTmp)
            customerTmp.email = customer.email
            customerTmp.firstName = customer.firstName
            customerTmp.lastName = customer.lastName
            customerTmp.telephone = customer.telephone
            println(customerTmp)
            customerRepository.save(customerTmp)
        }
        return customerTmp
    }
}
