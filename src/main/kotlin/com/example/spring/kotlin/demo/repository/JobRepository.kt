package com.example.spring.kotlin.demo.repository

import com.example.spring.kotlin.demo.repository.entities.Job
import org.springframework.data.repository.PagingAndSortingRepository


interface JobRepository : PagingAndSortingRepository<Job, Long>


