package com.example.spring.kotlin.demo.repository.entities.datatable


/**
 * The Class DataTableColumnSpecs.
 *
 * @author pavan.solapure
 */
class DataTableColumnSpecs
/**
 * Instantiates a new data table column specs.
 *
 * @param request the request
 * @param i       the i
 */
/**
 * Prepare column specs.
 *
 * @param request the request
 * @param i       the i
 */
{
    /** The index.  */
    var index = 0

    /** The data.  */
    var data: String? = null
    /** The name.  */
    var name: String? = null
    /** The searchable.  */
    var isSearchable = false
    /** The orderable.  */
    var isOrderable = false
    /** The search.  */
    var search: String? = null
    /** The regex.  */
    var isRegex = false
    /** The sort dir.  */
    var sortDir: String? = null
}