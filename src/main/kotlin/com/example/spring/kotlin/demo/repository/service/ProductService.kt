package com.example.spring.kotlin.demo.repository.service

import com.example.spring.kotlin.demo.repository.ProductRepository
import com.example.spring.kotlin.demo.repository.entities.Product
import com.example.spring.kotlin.demo.repository.entities.datatable.DataTablePage
import com.example.spring.kotlin.demo.repository.entities.datatable.FilterBy
import org.springframework.context.annotation.Lazy
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Service
import java.util.*


@Service
class ProductService (@Lazy private val productRepository: ProductRepository) {

    fun findByID(id: Long): Optional<Product?> {
        return productRepository.findById(id)
    }

    fun getProductsPaginated(filterBy: FilterBy, start: Int, pageSize: Int,
                             orderClause: String?, orderDirection: String?): DataTablePage<Product?> {
        var start = start
        val page = DataTablePage<Product?>()
        val totalLong = productRepository.count()
        start = start / pageSize
        page.data = findProductsPaginated(start, pageSize, orderClause, orderDirection, filterBy.globalFilter)
        val totalFiltered = if (filterBy.isGlobalSearch) page.data!!.size else totalLong.toInt()
        page.recordsTotal = totalLong.toInt()
        page.recordsFiltered = totalFiltered
        return page
    }

    fun findProductsPaginated(start: Int?, pageSize: Int?, orderClause: String?,
                              orderDirection: String?, search: String?): List<Product?> {
        val paging: Pageable = PageRequest.of(start!!, pageSize!!, Sort.by(Sort.Direction.fromString(orderDirection!!), orderClause))
        if (null == search) {
            val pageResult = productRepository.findAll(paging)
            return if (pageResult.hasContent()) pageResult.content else ArrayList<Product>()
        }
        val products: MutableList<Product?> = LinkedList()
        productRepository.findAll(paging).forEach{
            if (it?.name!!.toLowerCase().contains(search.toLowerCase())) {
                products.add(it)
            }
        }
        return products
    }

    fun save(product: Product): Product {
        return productRepository.save(product)
    }

    fun deleteProduct(id: Long) {
        productRepository.deleteById(id)
    }

    fun editProduct(product: Product): Product? {
        val productDB = productRepository.findById(product.id!!)
        if (productDB.isPresent) {
            val productTmp = productDB.get()
            productTmp.name = product.name
            productRepository.save(productTmp)
            return productTmp
        }
        return null
    }
}
