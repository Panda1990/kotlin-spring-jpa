package com.example.spring.kotlin.demo.repository.service

import com.example.spring.kotlin.demo.repository.CommerceRepository
import com.example.spring.kotlin.demo.repository.entities.CommerceItem
import org.springframework.context.annotation.Lazy
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Service
import org.springframework.web.bind.annotation.RequestBody
import java.util.*


@Service
class CommerceService (@Lazy private val commerceRepository: CommerceRepository) {

    // Save Customer
    fun saveCommerceItem(@RequestBody commerceItem: CommerceItem): CommerceItem {
        return commerceRepository.save(commerceItem)
    }

    //Find by name
    fun findByName(name: String): List<CommerceItem> {
        return commerceRepository.findCommerceItemsByName(name)
    }

    // Find by description
    fun findByDescription(description: String): List<CommerceItem> {
        return commerceRepository.findCommerceItemsByDescription(description)
    }

    // Find By name or description
/*    fun findByNameOrDescription(toSearch: String): List<CommerceItem> {
        return commerceRepository.findCommerceItemsByNameOrDescription(toSearch)
    }*/

    fun findCommerceItemPaginated(start: Int?, pageSize: Int?, orderClause: String?,
                              orderDirection: String?, search: String?): List<CommerceItem?> {
        val paging: Pageable = PageRequest.of(start!!, pageSize!!, Sort.by(Sort.Direction.fromString(orderDirection!!), orderClause))
        if (null == search) {
            val pageResult = commerceRepository.findAll(paging)
            return if (pageResult.hasContent()) pageResult.content else ArrayList<CommerceItem>()
        }
        val commerceItems: MutableList<CommerceItem?> = LinkedList()
        commerceRepository.findAll(paging).forEach{
            if (it?.name!!.toLowerCase().contains(search.toLowerCase())) {
                commerceItems.add(it)
            }
        }
        return commerceItems
    }
}
