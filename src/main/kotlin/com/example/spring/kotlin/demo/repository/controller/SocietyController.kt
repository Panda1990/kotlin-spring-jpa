package com.example.spring.kotlin.demo.repository.controller

import com.example.spring.kotlin.demo.repository.entities.Society
import com.example.spring.kotlin.demo.repository.service.SocietyService
import org.springframework.context.annotation.Lazy
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*


@RestController
@RequestMapping("/rest/society")
class SocietyController (@Lazy private val societyService: SocietyService) {

    // http://localhost:8080/society?pageNum=5&pageSize=1&sortBy=CF
    @GetMapping
    fun getAllSociety(@RequestParam(defaultValue = "0") pageNum: Int?,
                      @RequestParam(defaultValue = "10") pageSize: Int?, @RequestParam(defaultValue = "CF") sortBy: String?): ResponseEntity<List<Society?>> {
        val societies= societyService.getAllSociety(pageNum, pageSize, sortBy)
        return ResponseEntity(societies, HttpHeaders(), HttpStatus.OK)
    }

    @GetMapping("/{name}")
    fun findSocietyByName(@PathVariable name: String?): ResponseEntity<Society> {
        val society = societyService.findByName(name)
        val status = if (society!!.isPresent) HttpStatus.OK else HttpStatus.NOT_FOUND
        return ResponseEntity(society?.get(), HttpHeaders(), status)
    }

    @PostMapping
    fun saveSociety(@RequestBody society: Society?): ResponseEntity<Society> {
        val societytmp = societyService.saveSociety(society!!)
        return ResponseEntity(societytmp, HttpHeaders(), HttpStatus.OK)
    }

    @PutMapping
    fun editSociety(@RequestBody society: Society?): ResponseEntity<Society> {
        println(society)
        val societyEdited = societyService.editSociety(society!!)
        val status = if (societyEdited?.isPresent!!) HttpStatus.OK else HttpStatus.NOT_FOUND
        return ResponseEntity(societyEdited.get(), HttpHeaders(), status)
    }

    @DeleteMapping("/{name}")
    fun deleteSociety(@PathVariable name: String?): ResponseEntity<String> {
        societyService.deleteSociety(name)
        return ResponseEntity("Deleted", HttpHeaders(), HttpStatus.OK)
    }
}
