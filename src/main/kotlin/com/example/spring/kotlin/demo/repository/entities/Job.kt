package com.example.spring.kotlin.demo.repository.entities

import java.sql.Date
import javax.persistence.*


@Entity
class Job {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Long? = null
    @OneToOne
    var customer: Customer? = null
    @OneToOne
    var product: Product? = null
    var description: String? = null
    var startJobDate: Date? = null
    var pickUpDate: Date? = null
    var completing: Int? = null
    var price: Double? = null

    constructor(customer: Customer?, product: Product?, description: String?, startJobDate: Date?, pickUpDate: Date?,
                completing: Int?, price: Double?) : super() {
        this.customer = customer
        this.product = product
        this.description = description
        this.startJobDate = startJobDate
        this.pickUpDate = pickUpDate
        this.completing = completing
        this.price = price
    }

    constructor() : super() { // TODO Auto-generated constructor stub
    }

    constructor(customer: Customer?, product: Product?, price: Double, startDate: Date?, pickUpDate: Date?, description: String?) {
        this.customer = customer
        this.product = product
        this.price = price
        startJobDate = startDate
        this.pickUpDate = pickUpDate
        this.description = description
        completing = 0
    }

    override fun toString(): String {
        return ("Job [ID=" + id + ", customer=" + customer + ", product=" + product + ", Description=" + description
                + ", startJobDate=" + startJobDate + ", pickUpDate=" + pickUpDate + ", completing=" + completing
                + ", price=" + price + "]")
    }
}
