package com.example.spring.kotlin.demo.repository.entities.datatable

import javax.servlet.http.HttpServletRequest


object DataTableUtils {
    fun prepareColumnSpecs(request: HttpServletRequest, i: Int): DataTableColumnSpecs {
        val spec = DataTableColumnSpecs()
        spec.index = i
        spec.data = request.getParameter("columns[$i][data]")
        spec.name = request.getParameter("columns[$i][name]")
        spec.isOrderable = java.lang.Boolean.valueOf(request.getParameter("columns[$i][orderable]"))
        spec.isRegex = java.lang.Boolean.valueOf(request.getParameter("columns[$i][search][regex]"))
        spec.search = request.getParameter("columns[$i][search][value]")
        spec.isSearchable = java.lang.Boolean.valueOf(request.getParameter("columns[$i][searchable]"))
        val sortableColumnString = request.getParameter("order[0][column]")
        if (sortableColumnString != null) {
            val sortableCol = sortableColumnString.toInt()
            val sortDir = request.getParameter("order[0][dir]")
            if (i == sortableCol) {
                spec.sortDir = sortDir
            }
        }
        return spec
    }

    /**
     * Checks if is collection empty.
     *
     * @param collection the collection
     * @return true, if is collection empty
     */
    private fun isCollectionEmpty(collection: Collection<*>?): Boolean {
        return if (collection == null || collection.isEmpty()) {
            true
        } else false
    }

    /**
     * Checks if is object empty.
     *
     * @param object the object
     * @return true, if is object empty
     */
    fun isObjectEmpty(`object`: Any?): Boolean {
        if (`object` == null) return true else if (`object` is String) {
            if (`object`.trim { it <= ' ' }.length == 0) {
                return true
            }
        } else if (`object` is Collection<*>) {
            return isCollectionEmpty(`object` as Collection<*>?)
        }
        return false
    }
}
