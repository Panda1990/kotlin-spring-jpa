package com.example.spring.kotlin.demo.repository.entities

import java.io.Serializable
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id


@Entity
class Customer : Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Long? = null
    var firstName: String? = null
    var lastName: String? = null
    var email: String? = null
    var telephone: String? = null

    constructor() { // TODO Auto-generated constructor stub
    }

    constructor(firstName: String?, lastName: String?, email: String?, telephone: String?) : super() {
        this.firstName = firstName
        this.lastName = lastName
        this.email = email
        this.telephone = telephone
    }

    override fun toString(): String {
        return "Customer[$id, firstName='$firstName', lastName='$lastName']"
    }

    companion object {
        /**
         *
         */
        const val serialversionuid = -818748332828613759L

    }
}
