package com.example.spring.kotlin.demo.repository.service

import com.example.spring.kotlin.demo.repository.CustomerRepository
import com.example.spring.kotlin.demo.repository.ProductRepository
import com.example.spring.kotlin.demo.repository.SocietyRepository
import com.example.spring.kotlin.demo.repository.entities.Customer
import com.example.spring.kotlin.demo.repository.entities.Product
import com.example.spring.kotlin.demo.repository.entities.Society
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Service
import java.util.*
import java.util.function.Consumer


@Service
class SocietyService (@Lazy private val societyRestRepository: SocietyRepository, @Lazy private val customerRestRepository: CustomerRepository, @Lazy private val productRestRepository: ProductRepository, @Lazy private val customerService: CustomerService) {

    fun saveSociety(society: Society): Society {
        society.customers!!.forEach(Consumer { customer: Customer -> customerRestRepository.save(customer) })
        society.products!!.forEach(Consumer { product: Product -> productRestRepository.save(product) })
        return societyRestRepository.save(society)
    }

    fun getAllSociety(pageNum: Int?, pageSize: Int?, sortBy: String?): List<Society?> {
        val paging: Pageable = PageRequest.of(pageNum!!, pageSize!!, Sort.by(sortBy))
        val pageResult = societyRestRepository.findAll(paging)
        return if (pageResult.hasContent()) pageResult.content else ArrayList<Society>()
    }

    fun editSociety(society: Society): Optional<Society> {
        val societyDB = societyRestRepository.findByName(society.name)
        if (societyDB!!.isPresent) { // Handle Customers
            val societyCustomers: MutableList<Customer> = LinkedList()
            society.customers!!.forEach(Consumer { customer: Customer ->
                val customers = customerService.findByFirstAndLastName(customer.firstName!!,
                        customer.lastName!!)
                if (customers.size > 0) {
                    societyCustomers.add(customers[0])
                } else {
                    societyCustomers.add(customerRestRepository.save(customer))
                }
            })
            // Handle Products
            val societyProducts: MutableList<Product> = LinkedList()
            society.products!!.forEach(Consumer { product: Product ->
                val productDB = productRestRepository.findByName(product.name)
                if (productDB!!.isPresent) {
                    societyProducts.add(productDB.get())
                } else {
                    societyProducts.add(productRestRepository.save(product))
                }
            })
            // Update Society
            societyDB.get().name = society.name
            societyDB.get().customers = societyCustomers
            societyDB.get().products = societyProducts
            societyRestRepository.save(societyDB.get())
            return Optional.of(societyDB.get())
        }
        return Optional.empty()
    }

    fun deleteSociety(name: String?) {
        societyRestRepository.delete(societyRestRepository.findByName(name)!!.get())
    }

    fun findByName(name: String?): Optional<Society?>? {
        return societyRestRepository.findByName(name)
    }
}
