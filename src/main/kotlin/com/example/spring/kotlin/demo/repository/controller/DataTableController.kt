package com.example.spring.kotlin.demo.repository.controller

import com.example.spring.kotlin.demo.repository.entities.Customer
import com.example.spring.kotlin.demo.repository.entities.Job
import com.example.spring.kotlin.demo.repository.entities.Product
import com.example.spring.kotlin.demo.repository.entities.datatable.DataTablePage
import com.example.spring.kotlin.demo.repository.entities.datatable.DataTableRequest
import com.example.spring.kotlin.demo.repository.entities.datatable.PaginationCriteria
import com.example.spring.kotlin.demo.repository.service.CustomerService
import com.example.spring.kotlin.demo.repository.service.JobService
import com.example.spring.kotlin.demo.repository.service.ProductService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


@RestController
@RequestMapping("/datatable")
class DataTableController (@Lazy private val jobService: JobService, @Lazy private val customerService: CustomerService, @Lazy private val productService: ProductService) {

    @GetMapping("/jobs")
    fun getJobs(request: HttpServletRequest?, response: HttpServletResponse?): DataTablePage<Job?> {
        val dataTableReq = DataTableRequest(request!!)
        val pagination= dataTableReq.paginationRequest
        val space= pagination.sortBy?.orderClause?.indexOf(" ")
        return jobService.getJobsPaginated(pagination.filterBy!!,
                pagination.start!!, pagination.pageSize!!,
                pagination.sortBy?.orderClause?.substring(0, space!!),
                pagination.sortBy?.orderClause?.substring(space!! + 1))
    }

    @GetMapping("/customers")
    fun getCustomers(request: HttpServletRequest?, response: HttpServletResponse?): DataTablePage<Customer?> {
        val dataTableReq = DataTableRequest(request!!)
        val pagination: PaginationCriteria = dataTableReq.paginationRequest
        val space: Int = pagination.sortBy!!.orderClause!!.indexOf(" ")
        return customerService.getCustomersPaginated(pagination.filterBy!!,
                pagination.start!!, pagination.pageSize!!,
                pagination.sortBy?.orderClause?.substring(0, space!!),
                pagination.sortBy?.orderClause?.substring(space!! + 1))
    }

    @GetMapping("/products")
    fun getProducts(request: HttpServletRequest?, response: HttpServletResponse?): DataTablePage<Product?> {
        val dataTableReq = DataTableRequest(request!!)
        val pagination: PaginationCriteria = dataTableReq.paginationRequest
        val space: Int = pagination.sortBy!!.orderClause!!.indexOf(" ")
        return productService.getProductsPaginated(pagination.filterBy!!,
                pagination.start!!, pagination.pageSize!!,
                pagination.sortBy?.orderClause?.substring(0, space!!),
                pagination.sortBy?.orderClause?.substring(space!! + 1))
    }
}
