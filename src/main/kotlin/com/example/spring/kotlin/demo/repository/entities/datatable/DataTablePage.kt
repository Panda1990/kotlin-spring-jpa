package com.example.spring.kotlin.demo.repository.entities.datatable

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlRootElement


@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
class DataTablePage<T> {

    var draw = 0
    var recordsTotal = 0
    var recordsFiltered = 0
    var data: List<T>? = null
}
