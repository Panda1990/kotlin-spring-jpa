package com.example.spring.kotlin.demo.repository.entities

import java.io.Serializable
import javax.persistence.*


@Entity
class Product : Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Long? = null
    @Column()
    var name: String? = null

    constructor() : super() { // TODO Auto-generated constructor stub
    }

    constructor(name: String?) : super() {
        this.name = name
    }

    companion object {
        /**
         *
         */
        const val serialversionuid = -6330899619667053818L

    }
}
