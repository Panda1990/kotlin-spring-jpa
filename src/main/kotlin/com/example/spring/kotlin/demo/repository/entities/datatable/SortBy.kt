package com.example.spring.kotlin.demo.repository.entities.datatable


/**
 * The Class SortBy.
 *
 * @author pavan.solapure
 */
class SortBy {
    /** The map of sorts.  */
    private var mapOfSorts: MutableMap<String, SortOrder>? = null

    /**
     * Gets the sort bys.
     *
     * @return the sortBys
     */
    val sortBys: Map<String, SortOrder>?
        get() = mapOfSorts

    /**
     * Adds the sort.
     *
     * @param sortBy the sort by
     */
    fun addSort(sortBy: String) {
        mapOfSorts!![sortBy] = SortOrder.ASC
    }

    /**
     * Adds the sort.
     *
     * @param sortBy    the sort by
     * @param sortOrder the sort order
     */
    fun addSort(sortBy: String, sortOrder: SortOrder) {
        mapOfSorts!![sortBy] = sortOrder
    }

    //Take just the first ordering
    val orderClause: String?
        get() { //Take just the first ordering
            val it: Iterator<String> = mapOfSorts!!.keys.iterator()
            return if (it.hasNext()) {
                val column = it.next()
                decodeColumnsName(column) + " " + mapOfSorts!![column]!!.value()
            } else {
                null
            }
        }

    private fun decodeColumnsName(columnName: String): String {
        println(columnName)
        when (columnName) {
            "pickupdate" -> return "pickUpDate"
            "startdate" -> return "startJobDate"
        }
        return columnName
    }

    /**
     * Instantiates a new sort by.
     */
    init {
        if (null == mapOfSorts) {
            mapOfSorts = HashMap()
        }
    }
}