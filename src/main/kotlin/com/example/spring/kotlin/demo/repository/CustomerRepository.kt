package com.example.spring.kotlin.demo.repository

import com.example.spring.kotlin.demo.repository.entities.Customer
import org.springframework.data.repository.PagingAndSortingRepository

interface CustomerRepository : PagingAndSortingRepository<Customer, Long> {
    fun findByLastName(lastName: String?): List<Customer?>?
    fun findByFirstName(firstName: String?): List<Customer?>?
    //fun findById(id: Long): Optional<Customer?>?
}
